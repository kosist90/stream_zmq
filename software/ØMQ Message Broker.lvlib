﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Sandbox" Type="Folder">
		<Item Name="AF Debug Tree.vi" Type="VI" URL="../Sandbox/AF Debug Tree.vi"/>
		<Item Name="Test Message Pack.vi" Type="VI" URL="../Sandbox/Test Message Pack.vi"/>
	</Item>
	<Item Name="String Look Up Table" Type="Folder">
		<Item Name="String Look Up Table.lvclass" Type="LVClass" URL="../Classes/String Look Up Table/String Look Up Table.lvclass"/>
	</Item>
	<Item Name="ØMQ Message Broker" Type="Folder">
		<Item Name="ØMQ Message Broker.lvclass" Type="LVClass" URL="../Classes/ØMQ Message Broker/ØMQ Message Broker.lvclass"/>
	</Item>
	<Item Name="ØMQ Message Broker Interface Ref" Type="Folder">
		<Item Name="ØMQ Message Broker Interface Ref.lvclass" Type="LVClass" URL="../Classes/ØMQ Message Broker Interface Ref/ØMQ Message Broker Interface Ref.lvclass"/>
	</Item>
	<Item Name="ØMQ Pub Socket Actor" Type="Folder">
		<Item Name="ØMQ Pub Socket Actor.lvclass" Type="LVClass" URL="../Classes/ØMQ Pub Socket Actor/ØMQ Pub Socket Actor.lvclass"/>
	</Item>
	<Item Name="ØMQ Server" Type="Folder">
		<Item Name="Msgs" Type="Folder">
			<Item Name="Launch Service Sub Socket Actors Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Server Messages/Launch Service Sub Socket Actors Msg/Launch Service Sub Socket Actors Msg.lvclass"/>
			<Item Name="Service Topic Alive Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Server Messages/Service Topic Alive Msg/Service Topic Alive Msg.lvclass"/>
			<Item Name="Service Topic Dead Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Server Messages/Service Topic Dead Msg/Service Topic Dead Msg.lvclass"/>
		</Item>
		<Item Name="ØMQ Server.lvclass" Type="LVClass" URL="../Classes/ØMQ Server/ØMQ Server.lvclass"/>
	</Item>
	<Item Name="ØMQ Service Sub Socket Actor" Type="Folder">
		<Item Name="ØMQ Service Sub Socket Actor.lvclass" Type="LVClass" URL="../Classes/ØMQ Service Sub Socket Actor/ØMQ Service Sub Socket Actor.lvclass"/>
	</Item>
	<Item Name="ØMQ Sub Socket Actor" Type="Folder">
		<Item Name="ØMQ Sub Socket Actor.lvclass" Type="LVClass" URL="../Classes/ØMQ Sub Socket Actor/ØMQ Sub Socket Actor.lvclass"/>
	</Item>
	<Item Name="ØMQ Topic Actor" Type="Folder">
		<Item Name="Msgs" Type="Folder">
			<Item Name="Handle Server Death Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor Messages/Handle Server Death Msg/Handle Server Death Msg.lvclass"/>
			<Item Name="Launch ØMQ Sub Socket Actors Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor Messages/Launch ØMQ Sub Socket Actors Msg/Launch ØMQ Sub Socket Actors Msg.lvclass"/>
			<Item Name="Luanch ØMQ Pub Socket Actor Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor Messages/Luanch ØMQ Pub Socket Actor Msg/Luanch ØMQ Pub Socket Actor Msg.lvclass"/>
			<Item Name="Send Alive Event Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor Messages/Send Alive Event Msg/Send Alive Event Msg.lvclass"/>
			<Item Name="ØMQ Server Topic Update Msg.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor Messages/ØMQ Server Topic Update Msg/ØMQ Server Topic Update Msg.lvclass"/>
		</Item>
		<Item Name="ØMQ Topic Actor.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Actor/ØMQ Topic Actor.lvclass"/>
	</Item>
	<Item Name="ØMQ Topic Look up Table" Type="Folder">
		<Item Name="ØMQ Topic Look Up Table.lvclass" Type="LVClass" URL="../Classes/ØMQ Topic Look Up Table/ØMQ Topic Look Up Table.lvclass"/>
	</Item>
</Library>
